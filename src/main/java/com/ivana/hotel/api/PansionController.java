package com.ivana.hotel.api;

import com.ivana.hotel.model.Pansion;
import com.ivana.hotel.service.PansionService;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/pansions")
@AllArgsConstructor
public class PansionController {

  @Autowired PansionService service;

  @GetMapping()
  public List<Pansion> findAll() {
    return service.findAll();
  }

  @PostMapping()
  public void newPansion(@RequestBody Pansion pansion) {
    service.save(pansion);
  }
}
