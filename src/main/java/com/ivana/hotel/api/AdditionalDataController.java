package com.ivana.hotel.api;

import com.ivana.hotel.model.AdditionalData;
import com.ivana.hotel.repository.AdditionalDataRepository;
import com.ivana.hotel.service.RoomService;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/data")
@AllArgsConstructor
public class AdditionalDataController {

  @Autowired private AdditionalDataRepository repository;

  @Autowired private RoomService roomService;

  @GetMapping("/dailyInit")
  void dailyInit() {
    List<AdditionalData> allData = repository.findAll();
    LocalDate today = LocalDate.now();
    LocalDateTime now = LocalDateTime.now();

    if (!allData.isEmpty()) {
      AdditionalData oldData = allData.get(0);

      LocalDate oldDate = oldData.getLastInit().toLocalDate();
      if (oldDate.isBefore(today)) {
        roomService.dailyInit();
        oldData.setLastInit(now);
        repository.save(oldData);
        return;
      }
    } else {
      AdditionalData data = new AdditionalData(now);
      repository.save(data);
    }
  }
}
