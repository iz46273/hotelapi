package com.ivana.hotel.api;

import com.ivana.hotel.dto.ReservationDto;
import com.ivana.hotel.model.Cleaning.Status;
import com.ivana.hotel.model.Reservation;
import com.ivana.hotel.service.ReservationService;
import com.ivana.hotel.service.RoomService;
import java.time.LocalDate;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/reservations")
@AllArgsConstructor
public class ReservationController {

  @Autowired ReservationService reservationService;
  @Autowired RoomService roomService;

  @GetMapping()
  public List<Reservation> findAll() {
    return reservationService.findAll();
  }

  @PostMapping("/update")
  public void update(@RequestBody Reservation reservation) {
    reservationService.update(reservation);
  }

  @PostMapping("/new")
  public void newReservation(@RequestBody ReservationDto reservationDto) {
    reservationService.newReservation(reservationDto);
  }

  @PostMapping("/newAndCheckIn")
  public void newAnCheckIn(@RequestBody ReservationDto reservationDto) {
    Reservation reservation = reservationService.newReservation(reservationDto);
    reservationService.checkIn(reservation.getId());
  }

  @GetMapping("/today/arrival")
  public List<Reservation> arrivalToday() {
    return reservationService.reservationsByStatusAndDate(Status.ARRIVAL, LocalDate.now());
  }

  @GetMapping("/today/departured")
  public List<Reservation> departureToday() {
    return reservationService.reservationsByStatusAndDate(Status.DEPARTURED, LocalDate.now());
  }

  @GetMapping("/today/dueOut")
  public List<Reservation> dueOutToday() {
    return reservationService.reservationsByStatusAndDate(Status.DUE_OUT, LocalDate.now());
  }

  @GetMapping("/today/stayover")
  public List<Reservation> stayoverToday() {
    return reservationService.reservationsByStatusAndDate(Status.STAYOVER, LocalDate.now());
  }

  @GetMapping("/today/vacant")
  public List<Reservation> vacantToday() {
    return reservationService.reservationsByStatusAndDate(Status.VACANT, LocalDate.now());
  }

  @GetMapping("/checkIn/{id}")
  public void checkIn(@PathVariable int id) {
    reservationService.checkIn(id);
  }

  @GetMapping("/checkOut/{id}")
  public void checkOut(@PathVariable int id) {
    reservationService.checkOut(id);
  }

  @GetMapping("/cancel/{id}")
  public void cancel(@PathVariable int id) {
    reservationService.cancel(id);
  }

  @GetMapping("/updateCheckOutDate/{id}/{date}")
  public void updateCheckOutDate(@PathVariable int id, @PathVariable String date) {
    LocalDate wantedDate = LocalDate.parse(date);
    reservationService.updateCheckOutDate(id, wantedDate);
  }
}
