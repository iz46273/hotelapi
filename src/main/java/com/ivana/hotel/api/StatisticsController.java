package com.ivana.hotel.api;

import com.ivana.hotel.dto.AdminStatistics;
import com.ivana.hotel.dto.HousekeepingStatistics;
import com.ivana.hotel.dto.ReceptionStatistics;
import com.ivana.hotel.service.StatisticsService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/statistics")
@AllArgsConstructor
public class StatisticsController {

  @Autowired StatisticsService service;

  @GetMapping("/admin")
  AdminStatistics getAdmin() {
    return service.getAdmin();
  }

  @GetMapping("/housekeeping")
  HousekeepingStatistics getHousekeeping() {
    return service.getHousekeeping();
  }

  @GetMapping("/reception")
  ReceptionStatistics getReception() {
    return service.getReception();
  }
}
