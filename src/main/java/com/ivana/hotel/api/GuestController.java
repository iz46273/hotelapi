package com.ivana.hotel.api;

import com.ivana.hotel.model.Guest;
import com.ivana.hotel.repository.GuestRepository;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/guests")
@AllArgsConstructor
public class GuestController {

  @Autowired private GuestRepository repository;

  @GetMapping()
  public List<Guest> findAll() {
    return repository.findAll();
  }

  @GetMapping("/getByEmail/{email}")
  public Guest findByEmail(@PathVariable String email) {
    return repository.findByEmail(email);
  }

  @GetMapping("/checkIfExists/{email}")
  public Boolean checkIfExists(@PathVariable String email) {
    Guest guest = repository.findByEmail(email);
    if (guest != null) return true;
    return false;
  }

  @PostMapping("/update")
  public void update(@RequestBody Guest guest) {
    repository.save(guest);
  }
}
