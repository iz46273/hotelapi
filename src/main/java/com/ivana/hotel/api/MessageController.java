package com.ivana.hotel.api;

import com.ivana.hotel.model.Message;
import com.ivana.hotel.repository.EmployeeRepository;
import com.ivana.hotel.repository.MessageRepository;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/messages")
@AllArgsConstructor
public class MessageController {

  @Autowired MessageRepository repository;

  @Autowired EmployeeRepository employeeRepository;

  @GetMapping()
  public List<Message> findAll() {
    return repository.findAll();
  }

  @GetMapping("/{id}")
  public List<Message> findByTo(@PathVariable int id) {
    List<Message> allMessages = repository.findAll();
    List<Message> messages = new ArrayList<>();
    System.out.println(id);

    for (int i = 0; i < allMessages.size(); i++) {
      System.out.println(allMessages.get(i).getTo().getUsername());
      if (!allMessages.get(i).isConfirmed() && allMessages.get(i).getTo().getId() == id)
        messages.add(allMessages.get(i));
    }
    return messages;
  }

  @PostMapping()
  public void newMessage(@RequestBody Message message) {
    message.setTime(LocalDateTime.now());
    repository.save(message);
  }

  @GetMapping("/read/{messageId}")
  public ResponseEntity<Message> readMessage(@PathVariable int messageId) {
    Message message = repository.findById(messageId);
    if (message != null) {
      message.setConfirmed(true);
      repository.save(message);
      return new ResponseEntity<>(HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
  }
}
