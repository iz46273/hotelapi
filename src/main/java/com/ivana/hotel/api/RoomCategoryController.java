package com.ivana.hotel.api;

import com.ivana.hotel.model.RoomCategory;
import com.ivana.hotel.repository.RoomCategoryRepository;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/categories")
@AllArgsConstructor
public class RoomCategoryController {

  @Autowired private RoomCategoryRepository repository;

  @GetMapping()
  public List<RoomCategory> getAll() {
    return repository.findAll();
  }

  @PostMapping
  RoomCategory save(@RequestBody RoomCategory roomCategory) {
    System.out.println(roomCategory.getBalcony());
    return repository.save(roomCategory);
  }
}
