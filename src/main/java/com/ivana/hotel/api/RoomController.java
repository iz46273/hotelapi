package com.ivana.hotel.api;

import com.ivana.hotel.model.Room;
import com.ivana.hotel.repository.RoomRepository;
import com.ivana.hotel.service.RoomService;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/rooms")
@AllArgsConstructor
public class RoomController {

  private final RoomService service;

  @Autowired private RoomRepository repository;

  @Autowired
  RoomController(RoomService service) {
    this.service = service;
  }

  @GetMapping()
  public List<Room> getAllRooms() {
    return service.findAll();
  }

  @DeleteMapping("/{roomId}")
  public void delete(@PathVariable int roomId) {
    repository.deleteById(roomId);
  }

  @GetMapping("/open")
  public List<Room> getOpenRooms() {
    return service.findOpen();
  }

  @GetMapping("/getStayPrice")
  public double getStayBasePrice(
      @RequestParam String from, @RequestParam String to, @RequestParam int roomId) {
    return service.getStayBasePrice(from, to, roomId);
  }

  @GetMapping("/open/filter")
  public List<Room> getFilteredOpenRooms(
      @RequestParam(required = false) String from,
      @RequestParam(required = false) String to,
      @RequestParam Integer capacity,
      @RequestParam String type,
      @RequestParam String clean) {

    List<Room> filtered = null;

    if (from != null && to != null && !from.isBlank() && !to.isBlank()) {
      if (!from.equals("undefined") && !to.equals("undefined")) {
        LocalDate start = LocalDate.parse(from);
        LocalDate end = LocalDate.parse(to);
        filtered = service.filterDates(start, end, capacity, type, clean);
      }
    }
    if (filtered == null) filtered = service.filterWithoutDates(capacity, type, clean);

    return filterOpenRooms(filtered);
  }

  @GetMapping("/filter")
  public List<Room> getFilteredRooms(
      @RequestParam(required = false) String from,
      @RequestParam(required = false) String to,
      @RequestParam Integer capacity,
      @RequestParam String type,
      @RequestParam String clean) {

    if (from != null && to != null && !from.isBlank() && !to.isBlank()) {
      if (!from.equals("undefined") && !to.equals("undefined")) {
        LocalDate start = LocalDate.parse(from);
        LocalDate end = LocalDate.parse(to);
        return service.filterDates(start, end, capacity, type, clean);
      }
    }
    return service.filterWithoutDates(capacity, type, clean);
  }

  @PostMapping("/update")
  ResponseEntity<Room> update(@RequestBody Room room) {
    return service.update(room);
  }

  private List<Room> filterOpenRooms(List<Room> filtered) {
    List<Room> returnRooms = new ArrayList<>();
    for (int i = 0; i < filtered.size(); i++) {
      if (filtered.get(i).getOpen().equals(true)) returnRooms.add(filtered.get(i));
    }
    return returnRooms;
  }

  @PostMapping()
  Room newRoom(@RequestBody Room room) {
    return repository.save(room);
  }

  @GetMapping("/getFirstUnavailableDate/{id}/{date}")
  public LocalDate getFirstUnavailableDate(@PathVariable int id, @PathVariable String date) {
    LocalDate wantedDate = LocalDate.parse(date);
    return service.getFirstUnavailableDate(id, wantedDate);
  }
}
