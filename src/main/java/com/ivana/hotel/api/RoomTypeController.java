package com.ivana.hotel.api;

import com.ivana.hotel.model.RoomType;
import com.ivana.hotel.repository.RoomTypeRepository;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/roomTypes")
@AllArgsConstructor
public class RoomTypeController {

  @Autowired private RoomTypeRepository repository;

  @GetMapping()
  List<RoomType> getAll() {
    return repository.findAll();
  }
}
