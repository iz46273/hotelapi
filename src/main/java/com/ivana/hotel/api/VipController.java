package com.ivana.hotel.api;

import com.ivana.hotel.model.Vip;
import com.ivana.hotel.repository.VipRepository;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/vip")
@AllArgsConstructor
public class VipController {

  @Autowired private VipRepository repository;

  @GetMapping()
  public List<Vip> findAll() {
    return repository.findAll();
  }
}
