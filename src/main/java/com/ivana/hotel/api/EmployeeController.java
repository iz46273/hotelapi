package com.ivana.hotel.api;

import com.ivana.hotel.model.Employee;
import com.ivana.hotel.service.EmployeeService;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/employees")
@AllArgsConstructor
public class EmployeeController {

  @Autowired EmployeeService service;

  @GetMapping("/getMaids")
  public List<Employee> getMaids() {
    return service.getMaids();
  }

  @GetMapping()
  public List<Employee> findAll() {
    return service.findAll();
  }

  @DeleteMapping("/{id}")
  public void delete(@PathVariable int id) {
    service.deleteById(id);
  }

  @GetMapping("/filter")
  public List<Employee> getMaids(@RequestParam String active, @RequestParam String position) {
    return service.filter(active, position);
  }

  @PostMapping("/update")
  Employee update(@RequestBody Employee employee) {
    return service.update(employee);
  }
}
