package com.ivana.hotel.api;

import com.ivana.hotel.dto.CleaningDto;
import com.ivana.hotel.model.Cleaning;
import com.ivana.hotel.service.CleaningService;
import java.time.LocalDate;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/cleaning")
@AllArgsConstructor
public class CleaningController {

  @Autowired CleaningService service;

  @PostMapping("/new")
  void newCleaning(@RequestBody List<Cleaning> cleaning) {
    service.newCleaning(cleaning);
  }

  @GetMapping("/checkIfGenerated")
  boolean checkIfGenerated() {
    return service.checkIfGenerated();
  }

  @GetMapping("/getToday")
  List<CleaningDto> getToday() {
    return service.getToday();
  }

  @GetMapping("/getByDate/{date}")
  List<CleaningDto> getByDate(@PathVariable String date) {
    if (date != null && !date.isBlank()) {
      if (!date.equals("undefined")) {
        LocalDate wantedDate = LocalDate.parse(date);
        return service.getByDate(wantedDate);
      }
    }
    return null;
  }

  @PostMapping("/update")
  ResponseEntity<Cleaning> update(@RequestBody Cleaning cleaning) {
    return service.update(cleaning);
  }
}
