package com.ivana.hotel.api;

import com.ivana.hotel.dto.LoginRequest;
import com.ivana.hotel.model.Employee;
import com.ivana.hotel.repository.EmployeeRepository;
import com.ivana.hotel.security.JwtResponse;
import com.ivana.hotel.security.JwtUtils;
import com.ivana.hotel.security.UserDetailsImpl;
import com.ivana.hotel.service.EmployeeService;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {

  @Autowired AuthenticationManager authenticationManager;

  @Autowired EmployeeRepository repository;

  @Autowired EmployeeService service;

  @Autowired PasswordEncoder encoder;

  @Autowired JwtUtils jwtUtils;

  @PostMapping("/login")
  public ResponseEntity<?> authenticateuser(@RequestBody LoginRequest loginRequest) {

    Employee employee = repository.findByUsername(loginRequest.getUsername());
    if (employee != null) {
      if (employee.getActive().equals(true)) {
        Authentication authentication =
            authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                    loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        JwtResponse jwtResponse =
            new JwtResponse(
                jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getFirstName(),
                userDetails.getLastName(),
                userDetails.getPosition());
        System.out.println(jwtResponse.getAccessToken() + ", " + jwtResponse.getUsername());
        return ResponseEntity.ok(jwtResponse);
      } else return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
    } else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
  }

  @PostMapping("/register")
  ResponseEntity<Employee> update(@RequestBody Map<String, String> json) {
    String username = json.get("username");
    String password = json.get("password");
    return service.register(username, password);
  }
}
