package com.ivana.hotel.dto;

public class AdminStatistics {

  private double currentOccupancy;

  private double monthlyOccupancy;

  private double monthlyOccupancyDifference;

  private double monthlyEarnings;

  private double monthlyEarningsDifference;

  private double yearlyEarnings;

  private double yearlyEarningsDifference;

  private double averageNightsPerStay;

  public AdminStatistics() {
    super();
    // TODO Auto-generated constructor stub
  }

  public double getCurrentOccupancy() {
    return currentOccupancy;
  }

  public void setCurrentOccupancy(double currentOccupancy) {
    this.currentOccupancy = currentOccupancy;
  }

  public double getMonthlyOccupancy() {
    return monthlyOccupancy;
  }

  public void setMonthlyOccupancy(double monthlyOccupancy) {
    this.monthlyOccupancy = monthlyOccupancy;
  }

  public double getMonthlyOccupancyDifference() {
    return monthlyOccupancyDifference;
  }

  public void setMonthlyOccupancyDifference(double monthlyOccupancyDifference) {
    this.monthlyOccupancyDifference = monthlyOccupancyDifference;
  }

  public double getMonthlyEarnings() {
    return monthlyEarnings;
  }

  public void setMonthlyEarnings(double monthlyEarnings) {
    this.monthlyEarnings = monthlyEarnings;
  }

  public double getMonthlyEarningsDifference() {
    return monthlyEarningsDifference;
  }

  public void setMonthlyEarningsDifference(double monthlyEarningsDifference) {
    this.monthlyEarningsDifference = monthlyEarningsDifference;
  }

  public double getYearlyEarnings() {
    return yearlyEarnings;
  }

  public void setYearlyEarnings(double yearlyEarnings) {
    this.yearlyEarnings = yearlyEarnings;
  }

  public double getYearlyEarningsDifference() {
    return yearlyEarningsDifference;
  }

  public void setYearlyEarningsDifference(double yearlyEarningsDifference) {
    this.yearlyEarningsDifference = yearlyEarningsDifference;
  }

  public double getAverageNightsPerStay() {
    return averageNightsPerStay;
  }

  public void setAverageNightsPerStay(double averageNightsPerStay) {
    this.averageNightsPerStay = averageNightsPerStay;
  }
}
