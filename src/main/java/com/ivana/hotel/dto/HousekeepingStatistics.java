package com.ivana.hotel.dto;

public class HousekeepingStatistics {

  private int maidsWorkingToday;

  private int allMaids;

  private int allRoomsToClean;

  private int cleanedRooms;

  private double averageRoomsPerMaid;

  private int checkedOutPercentage;

  private int arrivalPercentage;

  public HousekeepingStatistics() {
    super();
    // TODO Auto-generated constructor stub
  }

  public int getMaidsWorkingToday() {
    return maidsWorkingToday;
  }

  public void setMaidsWorkingToday(int maidsWorkingToday) {
    this.maidsWorkingToday = maidsWorkingToday;
  }

  public int getAllMaids() {
    return allMaids;
  }

  public void setAllMaids(int allMaids) {
    this.allMaids = allMaids;
  }

  public int getAllRoomsToClean() {
    return allRoomsToClean;
  }

  public void setAllRoomsToClean(int allRoomsToClean) {
    this.allRoomsToClean = allRoomsToClean;
  }

  public int getCleanedRooms() {
    return cleanedRooms;
  }

  public void setCleanedRooms(int cleanedRooms) {
    this.cleanedRooms = cleanedRooms;
  }

  public double getAverageRoomsPerMaid() {
    return averageRoomsPerMaid;
  }

  public void setAverageRoomsPerMaid(double averageRoomsPerMaid) {
    this.averageRoomsPerMaid = averageRoomsPerMaid;
  }

  public int getCheckedOutPercentage() {
    return checkedOutPercentage;
  }

  public void setCheckedOutPercentage(int checkedOutPercentage) {
    this.checkedOutPercentage = checkedOutPercentage;
  }

  public int getArrivalPercentage() {
    return arrivalPercentage;
  }

  public void setArrivalPercentage(int arrivalPercentage) {
    this.arrivalPercentage = arrivalPercentage;
  }
}
