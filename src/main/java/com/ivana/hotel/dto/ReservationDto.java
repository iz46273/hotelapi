package com.ivana.hotel.dto;

import com.ivana.hotel.model.Employee;
import com.ivana.hotel.model.Pansion;
import com.ivana.hotel.model.Room;

public class ReservationDto {

  private GuestDto guest;

  private int adults;

  private int babies;

  private int children;

  private String checkInDate;

  private String checkOutDate;

  private String housekeepingNote;

  private String receptionNote;

  private Pansion pansion;

  private Room room;

  private double totalPrice;

  private Employee receptionist;

  public ReservationDto() {
    super();
    // TODO Auto-generated constructor stub
  }

  public ReservationDto(
      GuestDto guest,
      int adults,
      int babies,
      int children,
      String checkInDate,
      String checkOutDate,
      String housekeepingNote,
      String receptionNote,
      Pansion pansion,
      Room room,
      double totalPrice,
      Employee receptionist) {
    super();
    this.guest = guest;
    this.adults = adults;
    this.babies = babies;
    this.children = children;
    this.checkInDate = checkInDate;
    this.checkOutDate = checkOutDate;
    this.housekeepingNote = housekeepingNote;
    this.receptionNote = receptionNote;
    this.pansion = pansion;
    this.room = room;
    this.totalPrice = totalPrice;
    this.receptionist = receptionist;
  }

  public GuestDto getGuest() {
    return guest;
  }

  public void setGuest(GuestDto guest) {
    this.guest = guest;
  }

  public int getAdults() {
    return adults;
  }

  public void setAdults(int adults) {
    this.adults = adults;
  }

  public int getBabies() {
    return babies;
  }

  public void setBabies(int babies) {
    this.babies = babies;
  }

  public int getChildren() {
    return children;
  }

  public void setChildren(int children) {
    this.children = children;
  }

  public String getCheckInDate() {
    return checkInDate;
  }

  public void setCheckInDate(String checkInDate) {
    this.checkInDate = checkInDate;
  }

  public String getCheckOutDate() {
    return checkOutDate;
  }

  public void setCheckOutDate(String checkOutDate) {
    this.checkOutDate = checkOutDate;
  }

  public String getHousekeepingNote() {
    return housekeepingNote;
  }

  public void setHousekeepingNote(String housekeepingNote) {
    this.housekeepingNote = housekeepingNote;
  }

  public String getReceptionNote() {
    return receptionNote;
  }

  public void setReceptionNote(String receptionNote) {
    this.receptionNote = receptionNote;
  }

  public Pansion getPansion() {
    return pansion;
  }

  public void setPansion(Pansion pansion) {
    this.pansion = pansion;
  }

  public Room getRoom() {
    return room;
  }

  public void setRoom(Room room) {
    this.room = room;
  }

  public double getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(double totalPrice) {
    this.totalPrice = totalPrice;
  }

  public Employee getReceptionist() {
    return receptionist;
  }

  public void setReceptionist(Employee receptionist) {
    this.receptionist = receptionist;
  }
}
