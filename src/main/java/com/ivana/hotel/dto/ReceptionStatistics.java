package com.ivana.hotel.dto;

public class ReceptionStatistics {

  private int availableRooms;

  private double occupied;

  private int guestsInHotel;

  private int checkedOutPercentage;

  private int arrivalPercentage;

  private int toArrive;

  private int dueOut;

  private int cleanedPercentage;

  public ReceptionStatistics() {
    super();
    // TODO Auto-generated constructor stub
  }

  public int getAvailableRooms() {
    return availableRooms;
  }

  public void setAvailableRooms(int availableRooms) {
    this.availableRooms = availableRooms;
  }

  public double getOccupied() {
    return occupied;
  }

  public void setOccupied(double occupied) {
    this.occupied = occupied;
  }

  public int getGuestsInHotel() {
    return guestsInHotel;
  }

  public void setGuestsInHotel(int guestsInHotel) {
    this.guestsInHotel = guestsInHotel;
  }

  public int getCheckedOutPercentage() {
    return checkedOutPercentage;
  }

  public void setCheckedOutPercentage(int checkedOutPercentage) {
    this.checkedOutPercentage = checkedOutPercentage;
  }

  public int getArrivalPercentage() {
    return arrivalPercentage;
  }

  public void setArrivalPercentage(int arrivalPercentage) {
    this.arrivalPercentage = arrivalPercentage;
  }

  public int getToArrive() {
    return toArrive;
  }

  public void setToArrive(int toArrive) {
    this.toArrive = toArrive;
  }

  public int getDueOut() {
    return dueOut;
  }

  public void setDueOut(int dueOut) {
    this.dueOut = dueOut;
  }

  public int getCleanedPercentage() {
    return cleanedPercentage;
  }

  public void setCleanedPercentage(int cleanedPercentage) {
    this.cleanedPercentage = cleanedPercentage;
  }
}
