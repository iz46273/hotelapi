package com.ivana.hotel.dto;

import com.ivana.hotel.model.Employee;
import com.ivana.hotel.model.Reservation;
import java.time.LocalDate;

public class CleaningDto {

  private Employee maid;

  private Reservation reservation;

  private LocalDate date;

  private boolean arrival;

  private boolean dueOut;

  private boolean departured;

  private boolean stayover;

  private boolean vacant;

  private String maidNote;

  public CleaningDto(
      Employee maid,
      Reservation reservation,
      LocalDate date,
      boolean arrival,
      boolean dueOut,
      boolean departured,
      boolean stayover,
      boolean vacant,
      String maidNote) {
    super();
    this.maid = maid;
    this.reservation = reservation;
    this.date = date;
    this.arrival = arrival;
    this.dueOut = dueOut;
    this.departured = departured;
    this.stayover = stayover;
    this.vacant = vacant;
    this.maidNote = maidNote;
  }

  public CleaningDto() {
    super();
    // TODO Auto-generated constructor stub
  }

  public Employee getMaid() {
    return maid;
  }

  public void setMaid(Employee maid) {
    this.maid = maid;
  }

  public Reservation getReservation() {
    return reservation;
  }

  public void setReservation(Reservation reservation) {
    this.reservation = reservation;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public boolean isArrival() {
    return arrival;
  }

  public void setArrival(boolean arrival) {
    this.arrival = arrival;
  }

  public boolean isDueOut() {
    return dueOut;
  }

  public void setDueOut(boolean dueOut) {
    this.dueOut = dueOut;
  }

  public boolean isDepartured() {
    return departured;
  }

  public void setDepartured(boolean departured) {
    this.departured = departured;
  }

  public boolean isStayover() {
    return stayover;
  }

  public void setStayover(boolean stayover) {
    this.stayover = stayover;
  }

  public String getMaidNote() {
    return maidNote;
  }

  public void setMaidNote(String maidNote) {
    this.maidNote = maidNote;
  }

  public boolean isVacant() {
    return vacant;
  }

  public void setVacant(boolean vacant) {
    this.vacant = vacant;
  }
}
