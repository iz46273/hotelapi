package com.ivana.hotel.dto;

import com.ivana.hotel.model.Vip;

public class GuestDto {

  private int id;

  private String country;

  private String email;

  private String firstName;

  private String lastName;

  private String passport;

  private String phone;

  private Vip vip;

  public GuestDto(
      int id,
      String country,
      String email,
      String firstName,
      String lastName,
      String passport,
      String phone,
      Vip vip) {
    super();
    this.id = id;
    this.country = country;
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
    this.passport = passport;
    this.phone = phone;
    this.vip = vip;
  }

  public GuestDto() {
    super();
    // TODO Auto-generated constructor stub
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getPassport() {
    return passport;
  }

  public void setPassport(String passport) {
    this.passport = passport;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public Vip getVip() {
    return vip;
  }

  public void setVip(Vip vip) {
    this.vip = vip;
  }
}
