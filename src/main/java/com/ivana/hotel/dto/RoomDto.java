package com.ivana.hotel.dto;

import com.ivana.hotel.model.Room;

public class RoomDto extends Room {

  private Boolean availableToday;

  private Boolean availableNow;

  private Integer availableForDays;

  public RoomDto(Room room) {
    this.id = room.getId();
    this.type = room.getType();
    this.category = room.getCategory();
    this.capacity = room.getCapacity();
    this.clean = room.getClean();
  }

  public Boolean getAvailableToday() {
    return availableToday;
  }

  public void setAvailableToday(Boolean availableToday) {
    this.availableToday = availableToday;
  }

  public Boolean getAvailableNow() {
    return availableNow;
  }

  public void setAvailableNow(Boolean availableNow) {
    this.availableNow = availableNow;
  }

  public Integer getAvailableForDays() {
    return availableForDays;
  }

  public void setAvailableForDays(Integer availableForDays) {
    this.availableForDays = availableForDays;
  }
}
