package com.ivana.hotel.repository;

import com.ivana.hotel.model.RoomType;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
@Component
public interface RoomTypeRepository extends JpaRepository<RoomType, Integer> {

  List<RoomType> findAll();
}
