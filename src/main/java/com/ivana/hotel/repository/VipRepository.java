package com.ivana.hotel.repository;

import com.ivana.hotel.model.Vip;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
@Component
public interface VipRepository extends JpaRepository<Vip, Integer> {

  List<Vip> findAll();
}
