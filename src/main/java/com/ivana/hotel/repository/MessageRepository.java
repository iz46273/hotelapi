package com.ivana.hotel.repository;

import com.ivana.hotel.model.Employee;
import com.ivana.hotel.model.Message;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepository extends JpaRepository<Message, Integer> {

  List<Message> findAll();

  Message findById(int id);

  List<Message> findByTo(Employee to);
}
