package com.ivana.hotel.repository;

import com.ivana.hotel.model.Guest;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
@Component
public interface GuestRepository extends JpaRepository<Guest, Integer> {

  List<Guest> findAll();

  Guest findByEmail(String email);
}
