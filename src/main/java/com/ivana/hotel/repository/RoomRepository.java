package com.ivana.hotel.repository;

import com.ivana.hotel.model.Room;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
@Component
public interface RoomRepository extends JpaRepository<Room, Integer> {

  List<Room> findAll();

  Room findById(int id);

  List<Room> findAllByOpen(boolean b);

  List<Room> findAllByClean(boolean clean);
}
