package com.ivana.hotel.repository;

import com.ivana.hotel.model.Cleaning;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
@Component
public interface CleaningRepository extends JpaRepository<Cleaning, Integer> {

  List<Cleaning> findAll();

  List<Cleaning> findByDate(LocalDate date);
}
