package com.ivana.hotel.repository;

import com.ivana.hotel.model.Employee;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

  List<Employee> findAll();

  Employee findByUsername(String username);

  Boolean existsByUsername(String username);

  Boolean existsByEmail(String email);
}
