package com.ivana.hotel.repository;

import com.ivana.hotel.model.Reservation;
import com.ivana.hotel.model.Room;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
@Component
public interface ReservationRepository extends JpaRepository<Reservation, Integer> {

  List<Reservation> findAll();

  Reservation findById(int id);

  List<Reservation> findByRoom(Room room);
}
