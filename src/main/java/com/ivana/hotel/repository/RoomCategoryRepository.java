package com.ivana.hotel.repository;

import com.ivana.hotel.model.RoomCategory;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
@Component
public interface RoomCategoryRepository extends JpaRepository<RoomCategory, Integer> {

  List<RoomCategory> findAll();
}
