package com.ivana.hotel.security;

import com.ivana.hotel.model.Employee;
import com.ivana.hotel.repository.EmployeeRepository;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
  @Autowired EmployeeRepository userRepository;

  @Override
  @Transactional
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Employee user;
    if (userRepository.existsByUsername(username)) {
      user = userRepository.findByUsername(username);
    } else throw new UsernameNotFoundException("user Not Found with username: " + username);
    return UserDetailsImpl.build(user);
  }
}
