package com.ivana.hotel.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class RoomType {

  public RoomType(String name, String description) {
    super();
    this.name = name;
    this.description = description;
  }

  @Id private String name;

  @Column(nullable = false)
  private String description;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "type")
  private List<Room> rooms;

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public RoomType() {
    super();
    // TODO Auto-generated constructor stub
  }
}
