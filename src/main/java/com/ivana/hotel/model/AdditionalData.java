package com.ivana.hotel.model;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class AdditionalData {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;

  @Column(nullable = false)
  private LocalDateTime lastDailyInit;

  public LocalDateTime getLastInit() {
    return lastDailyInit;
  }

  public AdditionalData() {
    super();
    // TODO Auto-generated constructor stub
  }

  public AdditionalData(LocalDateTime lastDailyInit) {
    super();
    this.lastDailyInit = lastDailyInit;
  }

  public void setLastInit(LocalDateTime lastDailyInit) {
    this.lastDailyInit = lastDailyInit;
  }
}
