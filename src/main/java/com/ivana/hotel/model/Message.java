package com.ivana.hotel.model;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Message {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;

  @Column(nullable = false)
  private String message;

  @Column(nullable = false)
  private LocalDateTime time;

  @ManyToOne()
  @JoinColumn(name = "sender_id", referencedColumnName = "id")
  private Employee from;

  @ManyToOne()
  @JoinColumn(name = "receiver_id", referencedColumnName = "id")
  private Employee to;

  @Column(nullable = false)
  private boolean confirmed;

  public Message(
      String message, LocalDateTime time, Employee from, Employee to, boolean confirmed) {
    super();
    this.message = message;
    this.time = time;
    this.from = from;
    this.to = to;
    this.confirmed = confirmed;
  }

  public Message() {
    super();
    // TODO Auto-generated constructor stub
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Employee getFrom() {
    return from;
  }

  public void setFrom(Employee from) {
    this.from = from;
  }

  public Employee getTo() {
    return to;
  }

  public void setTo(Employee to) {
    this.to = to;
  }

  public LocalDateTime getTime() {
    return time;
  }

  public void setTime(LocalDateTime time) {
    this.time = time;
  }

  public boolean isConfirmed() {
    return confirmed;
  }

  public void setConfirmed(boolean confirmed) {
    this.confirmed = confirmed;
  }
}
