package com.ivana.hotel.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Pansion {

  public Pansion(String id, Double price) {
    super();
    this.id = id;
    this.price = price;
  }

  @Id private String id;

  @Column(nullable = false)
  private Double price;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "pansion")
  private List<Reservation> reservations;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public Pansion() {
    super();
    // TODO Auto-generated constructor stub
  }
}
