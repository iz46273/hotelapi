package com.ivana.hotel.model;

import java.time.LocalDate;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
public class Employee {

  public Employee(
      Position position,
      String firstName,
      String lastName,
      String email,
      String username,
      String password,
      String phone) {
    this.position = position;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.username = username;
    this.password = password;
    this.phone = phone;
  }

  public enum Position {
    RECEPTION,
    HOUSEKEEPING,
    ADMINISTRATION,
    MAID
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @NotNull(message = "Employee must have s defined position.")
  @Column(nullable = false)
  private Position position;

  @NotEmpty(message = "Employee's first name cannot be empty.")
  @Column(nullable = false, length = 30)
  private String firstName;

  @NotEmpty(message = "Employee's last name cannot be empty.")
  @Column(nullable = false, length = 30)
  private String lastName;

  @Email
  @Column(nullable = true, length = 50, unique = true)
  private String email;

  @Column(nullable = true, length = 30, unique = true)
  private String username;

  @Column(nullable = true, length = 200)
  private String password;

  @NotEmpty(message = "Employee's phone number cannot be empty.")
  @Column(nullable = false, length = 30)
  private String phone;

  @NotNull(message = "Employment date cannot be null.")
  @Column(nullable = false)
  private LocalDate accountCreated;

  @Column(nullable = false, columnDefinition = "boolean default true")
  private Boolean active;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "receptionist")
  private List<Reservation> reservations;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "maid")
  private List<Cleaning> cleanings;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "from")
  private List<Message> messagesSent;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "to")
  private List<Message> messagesReceived;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Position getPosition() {
    return position;
  }

  public void setPosition(Position position) {
    this.position = position;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public LocalDate getAccountCreated() {
    return accountCreated;
  }

  public void setAccountCreated(LocalDate accountCreated) {
    this.accountCreated = accountCreated;
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public Employee() {
    super();
    // TODO Auto-generated constructor stub
  }
}
