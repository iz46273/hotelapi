package com.ivana.hotel.model;

import com.ivana.hotel.dto.GuestDto;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Entity
public class Guest {

  public Guest(
      @NotEmpty(message = "Guest's first name cannot be empty.") String firstName,
      @NotEmpty(message = "Guest's last name cannot be empty.") String lastName,
      @Email String email,
      String country,
      String passport,
      String phone,
      Vip vip) {
    super();
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.country = country;
    this.passport = passport;
    this.phone = phone;
    this.vip = vip;
  }

  public Guest(GuestDto guestDto) {
    super();
    this.firstName = guestDto.getFirstName();
    this.lastName = guestDto.getLastName();
    this.email = guestDto.getEmail();
    this.country = guestDto.getCountry();
    this.passport = guestDto.getPassport();
    this.phone = guestDto.getPhone();
    this.vip = guestDto.getVip();
  }

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;

  @NotEmpty(message = "Guest's first name cannot be empty.")
  @Column(nullable = false, length = 30)
  private String firstName;

  @NotEmpty(message = "Guest's last name cannot be empty.")
  @Column(nullable = false, length = 30)
  private String lastName;

  @Email
  @Column(nullable = true, length = 50, unique = true)
  private String email;

  @Column(nullable = true)
  private String country;

  @Column(nullable = true)
  private String passport;

  @Column(nullable = true)
  private String phone;

  @ManyToOne()
  @JoinColumn(name = "vip_id")
  private Vip vip;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "guest")
  private List<Reservation> reservations;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getPassport() {
    return passport;
  }

  public void setPassport(String passport) {
    this.passport = passport;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public Vip getVip() {
    return vip;
  }

  public void setVip(Vip vip) {
    this.vip = vip;
  }

  public Guest() {
    super();
    // TODO Auto-generated constructor stub
  }
}
