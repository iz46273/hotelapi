package com.ivana.hotel.model;

import com.ivana.hotel.dto.ReservationDto;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Reservation {
  public enum Status {
    BOOKED,
    CHECKEDIN,
    CHECKEDOUT,
    CANCELED
  }

  public Reservation(
      Room room,
      Guest guest,
      Employee receptionist,
      Status status,
      LocalDate checkInDate,
      LocalTime checkInTime,
      LocalDate checkOutDate,
      LocalTime checkOutTime,
      Pansion pansion,
      Double totalPrice,
      String receptionNote,
      String housekeepingNote,
      LocalDate createdDate,
      LocalTime createdTime,
      Integer adults,
      Integer children,
      Integer babies) {
    super();
    this.room = room;
    this.guest = guest;
    this.receptionist = receptionist;
    this.status = status;
    this.checkInDate = checkInDate;
    this.checkInTime = checkInTime;
    this.checkOutDate = checkOutDate;
    this.checkOutTime = checkOutTime;
    this.pansion = pansion;
    this.totalPrice = totalPrice;
    this.receptionNote = receptionNote;
    this.housekeepingNote = housekeepingNote;
    this.createdDate = createdDate;
    this.createdTime = createdTime;
    this.adults = adults;
    this.children = children;
    this.babies = babies;
  }

  public Reservation(ReservationDto reservationDto) {
    super();
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    this.room = reservationDto.getRoom();
    this.guest = null;
    this.receptionist = reservationDto.getReceptionist();
    this.status = Status.BOOKED;
    this.checkInDate = LocalDate.parse(reservationDto.getCheckInDate(), formatter);
    this.checkInTime = null;
    this.checkOutDate = LocalDate.parse(reservationDto.getCheckOutDate(), formatter);
    this.checkOutTime = null;
    this.pansion = reservationDto.getPansion();
    this.totalPrice = reservationDto.getTotalPrice();
    this.receptionNote = reservationDto.getReceptionNote();
    this.housekeepingNote = reservationDto.getHousekeepingNote();
    this.createdDate = LocalDate.now();
    this.createdTime = LocalTime.now();
    this.adults = reservationDto.getAdults();
    this.children = reservationDto.getChildren();
    this.babies = reservationDto.getBabies();
  }

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;

  @ManyToOne()
  @JoinColumn(name = "room_id")
  private Room room;

  @ManyToOne()
  @JoinColumn(name = "guest_id")
  private Guest guest;

  @ManyToOne()
  @JoinColumn(name = "employee_id")
  private Employee receptionist;

  @Column(nullable = false)
  private Status status;

  @Column(nullable = false)
  private LocalDate checkInDate;

  @Column(nullable = true)
  private LocalTime checkInTime;

  @Column(nullable = false)
  private LocalDate checkOutDate;

  @Column(nullable = true)
  private LocalTime checkOutTime;

  @ManyToOne()
  @JoinColumn(name = "pansion_id")
  private Pansion pansion;

  @Column(nullable = false)
  private Double totalPrice;

  @Column(nullable = true)
  private String receptionNote;

  @Column(nullable = true)
  private String housekeepingNote;

  @Column(nullable = false)
  private LocalDate createdDate;

  @Column(nullable = false)
  private LocalTime createdTime;

  @Column(nullable = false)
  private Integer adults;

  @Column(nullable = false)
  private Integer children;

  @Column(nullable = false)
  private Integer babies;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  public LocalDate getCheckInDate() {
    return checkInDate;
  }

  public void setCheckInDate(LocalDate checkInDate) {
    this.checkInDate = checkInDate;
  }

  public LocalDate getCheckOutDate() {
    return checkOutDate;
  }

  public void setCheckOutDate(LocalDate checkOutDate) {
    this.checkOutDate = checkOutDate;
  }

  public Pansion getPansion() {
    return pansion;
  }

  public void setPansion(Pansion pansion) {
    this.pansion = pansion;
  }

  public Double getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(Double totalPrice) {
    this.totalPrice = totalPrice;
  }

  public String getReceptionNote() {
    return receptionNote;
  }

  public void setReceptionNote(String receptionNote) {
    this.receptionNote = receptionNote;
  }

  public String getHousekeepingNote() {
    return housekeepingNote;
  }

  public void setHousekeepingNote(String housekeepingNote) {
    this.housekeepingNote = housekeepingNote;
  }

  public Employee getReceptionist() {
    return receptionist;
  }

  public void setReceptionist(Employee receptionist) {
    this.receptionist = receptionist;
  }

  public Room getRoom() {
    return room;
  }

  public void setRoom(Room room) {
    this.room = room;
  }

  public LocalTime getCheckInTime() {
    return checkInTime;
  }

  public void setCheckInTime(LocalTime checkInTime) {
    this.checkInTime = checkInTime;
  }

  public LocalTime getCheckOutTime() {
    return checkOutTime;
  }

  public void setCheckOutTime(LocalTime checkOutTime) {
    this.checkOutTime = checkOutTime;
  }

  public LocalDate getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(LocalDate createdDate) {
    this.createdDate = createdDate;
  }

  public LocalTime getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(LocalTime createdTime) {
    this.createdTime = createdTime;
  }

  public Guest getGuest() {
    return guest;
  }

  public void setGuest(Guest guest) {
    this.guest = guest;
  }

  public Integer getAdults() {
    return adults;
  }

  public void setAdults(Integer adults) {
    this.adults = adults;
  }

  public Integer getChildren() {
    return children;
  }

  public void setChildren(Integer children) {
    this.children = children;
  }

  public Integer getBabies() {
    return babies;
  }

  public void setBabies(Integer babies) {
    this.babies = babies;
  }

  public Reservation() {
    super();
    // TODO Auto-generated constructor stub
  }

  public int getNumberOfNights() {
    int number = 0;

    if (checkInDate != null && checkOutDate != null) {
      LocalDate tmp = checkInDate;

      while (tmp.isBefore(checkOutDate)) {
        number++;
        tmp = tmp.plusDays(1);
      }
    }

    return number;
  }
}
