package com.ivana.hotel.model;

import java.util.List;
import javax.persistence.*;
import lombok.Data;

@Entity
@Data
public class Room {

  public Room(
      int id,
      RoomType type,
      RoomCategory category,
      Integer capacity,
      Boolean clean,
      Boolean open,
      List<Reservation> reservations,
      List<Cleaning> cleanings) {
    super();
    this.id = id;
    this.type = type;
    this.category = category;
    this.capacity = capacity;
    this.clean = clean;
    this.open = open;
    this.reservations = reservations;
    this.cleanings = cleanings;
  }

  @Id protected int id;

  @ManyToOne()
  @JoinColumn(name = "roomType_id")
  protected RoomType type;

  @ManyToOne()
  @JoinColumn(name = "roomCategory_id")
  protected RoomCategory category;

  @Column(nullable = false)
  protected Integer capacity;

  @Column(nullable = false)
  protected Boolean clean;

  @Column(nullable = false)
  protected Boolean open;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "room")
  protected List<Reservation> reservations;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "room")
  protected List<Cleaning> cleanings;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public RoomType getType() {
    return type;
  }

  public void setType(RoomType type) {
    this.type = type;
  }

  public RoomCategory getCategory() {
    return category;
  }

  public void setCategory(RoomCategory category) {
    this.category = category;
  }

  public Integer getCapacity() {
    return capacity;
  }

  public void setCapacity(Integer capacity) {
    this.capacity = capacity;
  }

  public Boolean getClean() {
    return clean;
  }

  public void setClean(Boolean clean) {
    this.clean = clean;
  }

  public Boolean getOpen() {
    return open;
  }

  public void setOpen(Boolean open) {
    this.open = open;
  }

  public Room() {
    super();
    // TODO Auto-generated constructor stub
  }
}
