package com.ivana.hotel.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class RoomCategory {

  public RoomCategory(
      String name,
      Double size,
      Double price,
      Boolean balcony,
      Boolean hotTub,
      Boolean kitchenette,
      Boolean seaView) {
    super();
    this.name = name;
    this.size = size;
    this.price = price;
    this.balcony = balcony;
    this.hotTub = hotTub;
    this.kitchenette = kitchenette;
    this.seaView = seaView;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;

  @Column(nullable = false)
  private String name;

  @Column(nullable = false)
  private Double size;

  @Column(nullable = false)
  private Double price;

  @Column(nullable = false)
  private Boolean balcony;

  @Column(nullable = false)
  private Boolean hotTub;

  @Column(nullable = false)
  private Boolean kitchenette;

  @Column(nullable = false)
  private Boolean seaView;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "category")
  private List<Room> rooms;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Double getSize() {
    return size;
  }

  public void setSize(Double size) {
    this.size = size;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public Boolean getBalcony() {
    return balcony;
  }

  public void setBalcony(Boolean balcony) {
    this.balcony = balcony;
  }

  public Boolean getHotTub() {
    return hotTub;
  }

  public void setHotTub(Boolean hotTub) {
    this.hotTub = hotTub;
  }

  public Boolean getKitchenette() {
    return kitchenette;
  }

  public void setKitchenette(Boolean kitchenette) {
    this.kitchenette = kitchenette;
  }

  public Boolean getSeaView() {
    return seaView;
  }

  public void setSeaView(Boolean seaView) {
    this.seaView = seaView;
  }

  public RoomCategory() {
    super();
    // TODO Auto-generated constructor stub
  }
}
