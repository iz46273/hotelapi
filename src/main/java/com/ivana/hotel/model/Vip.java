package com.ivana.hotel.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Vip {

  public Vip(String id, String description) {
    super();
    this.id = id;
    this.description = description;
  }

  @Id private String id;

  @Column(nullable = false)
  private String description;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "vip")
  private List<Guest> guests;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Vip() {
    super();
    // TODO Auto-generated constructor stub
  }
}
