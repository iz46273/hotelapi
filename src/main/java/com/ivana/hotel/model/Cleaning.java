package com.ivana.hotel.model;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Cleaning {

  public enum Status {
    STAYOVER,
    DEPARTURED,
    ARRIVAL,
    DUE_OUT,
    VACANT
  }

  public Cleaning(Room room, LocalDate date, Employee maid, String note) {
    super();
    this.room = room;
    this.date = date;
    this.maid = maid;
    this.note = note;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;

  @ManyToOne()
  @JoinColumn(name = "room_id")
  private Room room;

  @Column(nullable = false)
  private LocalDate date;

  @ManyToOne()
  @JoinColumn(name = "employee_id")
  private Employee maid;

  @Column(nullable = true)
  private String note;

  public Room getRoom() {
    return room;
  }

  public void setRoom(Room room) {
    this.room = room;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public Employee getMaid() {
    return maid;
  }

  public void setMaid(Employee maid) {
    this.maid = maid;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public Cleaning() {
    super();
    // TODO Auto-generated constructor stub
  }
}
