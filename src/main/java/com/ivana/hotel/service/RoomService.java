package com.ivana.hotel.service;

import com.ivana.hotel.model.Reservation;
import com.ivana.hotel.model.Room;
import com.ivana.hotel.repository.RoomRepository;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RoomService {

  @Autowired private RoomRepository roomRepository;

  @Autowired private ReservationService reservationService;

  public List<Room> findAll() {
    List<Room> allRooms = roomRepository.findAll();
    return allRooms;
  }

  public List<Room> filterDates(
      LocalDate from, LocalDate to, Integer capacity, String type, String clean) {
    List<Room> allRooms = roomRepository.findAll();
    List<Room> returnRooms = new ArrayList<>();

    if (allRooms.isEmpty()) return returnRooms;

    for (int i = 0; i < allRooms.size(); i++) {
      if (checkIfAvailable(allRooms.get(i), from, to)
          && filter(allRooms.get(i), capacity, type, clean)) returnRooms.add(allRooms.get(i));
    }
    return returnRooms;
  }

  public List<Room> filterWithoutDates(Integer capacity, String type, String clean) {
    List<Room> allRooms = roomRepository.findAll();
    List<Room> returnRooms = new ArrayList<>();

    if (allRooms.isEmpty()) return returnRooms;

    for (int i = 0; i < allRooms.size(); i++) {
      if (filter(allRooms.get(i), capacity, type, clean)) returnRooms.add(allRooms.get(i));
    }
    return returnRooms;
  }

  private boolean filter(Room room, Integer capacity, String type, String clean) {
    boolean check = true;

    if (clean.equals("clean") && !room.getClean().equals(true)) check = false;
    if (clean.equals("dirty") && !room.getClean().equals(false)) check = false;
    if (room.getCapacity() < capacity) check = false;
    if (!type.equals("all") && !room.getType().getName().equals(type)) check = false;

    return check;
  }

  // TODO: check if CANCELED part returns correct value
  private boolean checkIfAvailable(Room room, LocalDate wantedCheckIn, LocalDate wantedCheckOut) {
    List<Reservation> allReservations = reservationService.findAll();
    Boolean check = true;

    for (int i = 0; i < allReservations.size(); i++) {
      if (allReservations.get(i).getRoom() == room) {
        LocalDate checkIn = allReservations.get(i).getCheckInDate();
        LocalDate checkOut = allReservations.get(i).getCheckOutDate();

        if (wantedCheckIn.isBefore(checkIn) && wantedCheckOut.isAfter(checkIn)
            || wantedCheckIn.isBefore(checkOut) && wantedCheckOut.isAfter(checkOut)
            || wantedCheckIn.isBefore(checkIn) && wantedCheckOut.isAfter(checkOut)
            || wantedCheckIn.isAfter(checkIn) && wantedCheckOut.isBefore(checkOut)
            || wantedCheckIn.equals(checkIn)
            || wantedCheckOut.equals(checkOut)) {

          if (!allReservations.get(i).getStatus().equals(Reservation.Status.CANCELED))
            check = false;
        }
      }
    }
    return check;
  }

  public double getStayBasePrice(String from, String to, int roomId) {
    Room room = roomRepository.findById(roomId);
    // TODO: add price variance
    return room.getCategory().getPrice();
  }

  // TODO: full update
  public ResponseEntity<Room> update(Room room) {
    Room oldRoom = roomRepository.findById(room.getId());
    if (oldRoom != null) {
      oldRoom.setClean(room.getClean());
      oldRoom.setCapacity(room.getCapacity());
      oldRoom.setCategory(room.getCategory());
      oldRoom.setOpen(room.getOpen());
      oldRoom.setType(room.getType());
      roomRepository.save(oldRoom);
      return new ResponseEntity<>(oldRoom, HttpStatus.OK);
    }
    return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
  }

  public void dailyInit() {
    LocalDate today = LocalDate.now();
    LocalDate yesterday = today.minusDays(1);
    List<Room> allRooms = roomRepository.findAll();

    for (int i = 0; i < allRooms.size(); i++) {
      Boolean check = checkIfAvailable(allRooms.get(i), yesterday, today);
      if (!check) {
        allRooms.get(i).setClean(false);
      }
    }
  }

  public List<Room> getOccupiedRooms(LocalDate today, LocalDate tomorrow) {
    List<Room> rooms = new ArrayList<>();
    List<Room> allRooms = roomRepository.findAll();

    for (int i = 0; i < allRooms.size(); i++) {
      Boolean check = checkIfAvailable(allRooms.get(i), today, tomorrow);
      if (!check) {
        rooms.add(allRooms.get(i));
      }
    }

    return rooms;
  }

  public List<Room> findOpen() {
    List<Room> returnRooms = roomRepository.findAllByOpen(true);
    return returnRooms;
  }

  public List<Room> findAllByClean(boolean clean) {
    List<Room> returnRooms = roomRepository.findAllByClean(clean);
    return returnRooms;
  }

  public LocalDate getFirstUnavailableDate(int id, LocalDate date) {
    Room room = roomRepository.findById(id);
    List<Reservation> reservations = reservationService.findByRoom(room);

    LocalDate firstDate = null;
    for (int i = 0; i < reservations.size(); i++) {
      if (firstDate != null) {
        if (reservations.get(i).getCheckInDate().isAfter(date)
            && reservations.get(i).getCheckInDate().isBefore(firstDate))
          firstDate = reservations.get(i).getCheckInDate();
      } else {
        if (reservations.get(i).getCheckInDate().isAfter(date))
          firstDate = reservations.get(i).getCheckInDate();
      }
    }

    if (firstDate != null) return firstDate;
    return date;
  }
}
