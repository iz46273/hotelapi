package com.ivana.hotel.service;

import com.ivana.hotel.dto.ReservationDto;
import com.ivana.hotel.model.Cleaning;
import com.ivana.hotel.model.Guest;
import com.ivana.hotel.model.Reservation;
import com.ivana.hotel.model.Reservation.Status;
import com.ivana.hotel.model.Room;
import com.ivana.hotel.repository.GuestRepository;
import com.ivana.hotel.repository.ReservationRepository;
import com.ivana.hotel.repository.RoomRepository;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ReservationService {

  @Autowired private ReservationRepository reservationRepository;

  @Autowired private GuestRepository guestRepository;

  @Autowired private RoomRepository roomRepository;

  public List<Reservation> findAll() {
    return reservationRepository.findAll();
  }

  public List<Reservation> getByDate(LocalDate today) {
    List<Reservation> allReservations = reservationRepository.findAll();
    List<Reservation> returnReservation = new ArrayList<>();

    for (int i = 0; i < allReservations.size(); i++) {
      Reservation reservation = allReservations.get(i);

      if (!reservation.getStatus().equals(Status.CANCELED)) {
        if (reservation.getCheckOutDate().equals(today)
            || (reservation.getCheckInDate().isBefore(today)
                && reservation.getCheckOutDate().isAfter(today))) {
          returnReservation.add(reservation);
        }

        if (reservation.getCheckInDate().equals(today)) {
          boolean check = true;
          for (int j = 0; j < returnReservation.size(); j++) {
            if (reservation.getRoom().getId() == returnReservation.get(j).getRoom().getId())
              check = false;
          }
          if (check) returnReservation.add(reservation);
        }
      }
    }
    return returnReservation;
  }

  public Reservation newReservation(ReservationDto reservationDto) {
    Reservation reservation = new Reservation(reservationDto);
    Guest guest = guestRepository.findByEmail(reservationDto.getGuest().getEmail());
    if (guest != null) reservation.setGuest(guest);
    else {
      guest = new Guest(reservationDto.getGuest());
      guestRepository.save(guest);
      reservation.setGuest(guest);
    }
    return reservationRepository.save(reservation);
  }

  public List<Reservation> reservationsByStatusAndDate(Cleaning.Status status, LocalDate date) {
    List<Reservation> allReservations = reservationRepository.findAll();
    List<Reservation> returnReservations = new ArrayList<>();

    switch (status) {
      case ARRIVAL:
        for (int i = 0; i < allReservations.size(); i++) {
          if (allReservations.get(i).getStatus() == Reservation.Status.BOOKED
              && allReservations.get(i).getCheckInDate().equals(date))
            returnReservations.add(allReservations.get(i));
        }
        break;
      case DEPARTURED:
        for (int i = 0; i < allReservations.size(); i++) {
          if (allReservations.get(i).getStatus() == Reservation.Status.CHECKEDOUT
              && allReservations.get(i).getCheckOutDate().equals(date))
            returnReservations.add(allReservations.get(i));
        }
        break;
      case DUE_OUT:
        for (int i = 0; i < allReservations.size(); i++) {
          if (allReservations.get(i).getStatus() == Reservation.Status.CHECKEDIN
              && allReservations.get(i).getCheckOutDate().equals(date))
            returnReservations.add(allReservations.get(i));
        }
        break;
      case STAYOVER:
        for (int i = 0; i < allReservations.size(); i++) {
          if (allReservations.get(i).getStatus() == Reservation.Status.CHECKEDIN
              && allReservations.get(i).getCheckInDate().isBefore(date)
              && allReservations.get(i).getCheckOutDate().isAfter(date))
            returnReservations.add(allReservations.get(i));
        }
        break;
      case VACANT:
        List<Reservation> today = getByDate(LocalDate.now());
        List<Room> dirtyRooms = roomRepository.findAllByClean(false);

        for (int i = 0; i < dirtyRooms.size(); i++) {
          boolean check = true;
          for (int j = 0; j < today.size(); j++) {
            if (dirtyRooms.get(i).getId() == today.get(j).getRoom().getId()
                || dirtyRooms.get(i).getOpen().equals(false)) check = false;
          }
          if (check) {
            Reservation res = new Reservation();
            res.setRoom(dirtyRooms.get(i));
            returnReservations.add(res);
            ;
          }
        }
        break;
      default:
        break;
    }

    return returnReservations;
  }

  public List<Reservation> getReservationsInPeriod(LocalDate endDate, int numberOfDaysInPeriod) {
    List<Reservation> allReservations = reservationRepository.findAll();
    List<Reservation> returnReservations = new ArrayList<>();

    for (int i = 0; i < allReservations.size(); i++) {
      if (allReservations.get(i).getCheckInDate().isBefore(endDate)
          && allReservations
              .get(i)
              .getCheckInDate()
              .isAfter(endDate.minusDays(numberOfDaysInPeriod + 1)))
        returnReservations.add(allReservations.get(i));
    }

    return returnReservations;
  }

  public void checkIn(int id) {
    Reservation reservation = reservationRepository.findById(id);
    reservation.setStatus(Status.CHECKEDIN);
    reservation.setCheckInTime(LocalTime.now());
    reservationRepository.save(reservation);
  }

  public void checkOut(int id) {
    Reservation reservation = reservationRepository.findById(id);
    reservation.setStatus(Status.CHECKEDOUT);
    reservation.setCheckOutTime(LocalTime.now());
    reservationRepository.save(reservation);
  }

  public void cancel(int id) {
    Reservation reservation = reservationRepository.findById(id);
    reservation.setStatus(Status.CANCELED);
    reservationRepository.save(reservation);
  }

  public void update(Reservation reservation) {
    reservationRepository.save(reservation);
  }

  public List<Reservation> findByRoom(Room room) {
    return reservationRepository.findByRoom(room);
  }

  public void updateCheckOutDate(int id, LocalDate wantedDate) {
    Reservation reservation = reservationRepository.findById(id);
    reservation.setCheckOutDate(wantedDate);
    reservationRepository.save(reservation);
  }
}
