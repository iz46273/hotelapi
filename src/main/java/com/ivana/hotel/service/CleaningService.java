package com.ivana.hotel.service;

import com.ivana.hotel.dto.CleaningDto;
import com.ivana.hotel.model.Cleaning;
import com.ivana.hotel.model.Cleaning.Status;
import com.ivana.hotel.model.Reservation;
import com.ivana.hotel.repository.CleaningRepository;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CleaningService {

  @Autowired private CleaningRepository repository;

  @Autowired private ReservationService reservationService;

  /*public List<CleaningDto> generate(List<Employee> maids) {
  	List<CleaningDto> cleanings = new ArrayList<>();
  	List<Reservation> stayover = reservationService.reservationsToday(Status.STAYOVER);
  	List<Reservation> departured = reservationService.reservationsToday(Status.DEPARTURED);
  	List<Reservation> arrival = reservationService.reservationsToday(Status.ARRIVAL);
  	List<Reservation> dueOut = reservationService.reservationsToday(Status.DUE_OUT);

  	List<Reservation> allReservations = new ArrayList<>();
  	allReservations.addAll(stayover);
  	allReservations.addAll(departured);
  	allReservations.addAll(arrival);
  	allReservations.addAll(dueOut);

  	allReservations = filterByDirty(allReservations);
  	allReservations = sortByRoom(allReservations);

  	int roomsPerMaid = Math.round(allReservations.size() / maids.size());
  	int res = 0;
  	for (var row = 0; row < maids.size(); row++) {
  		Employee maid = maids.get(row);
  		for (var col = 0; col < roomsPerMaid; col++) {
  			CleaningDto cleaning = new CleaningDto(maid, allReservations.get(res), LocalDate.now(), null);
  			if (stayover.contains(allReservations.get(res)))
  				cleaning.setStatus(Status.STAYOVER);
  			if (departured.contains(allReservations.get(res)))
  				cleaning.setStatus(Status.DEPARTURED);
  			if (arrival.contains(allReservations.get(res)))
  				cleaning.setStatus(Status.ARRIVAL);
  			if (dueOut.contains(allReservations.get(res)))
  				cleaning.setStatus(Status.DUE_OUT);
  			cleanings.add(cleaning);
  			res++;
  		}
  	}

  	int remainingRooms = allReservations.size() - res;
  	int maidIndex = 0;
  	for (int i = maids.size() - 1; i < remainingRooms; i--) {
  		CleaningDto cleaning = new CleaningDto(maids.get(maidIndex),
  				allReservations.get(allReservations.size() - 1 - i), LocalDate.now(), null);
  		if (stayover.contains(allReservations.get(res)))
  			cleaning.setStatus(Status.STAYOVER);
  		if (departured.contains(allReservations.get(res)))
  			cleaning.setStatus(Status.DEPARTURED);
  		if (arrival.contains(allReservations.get(res)))
  			cleaning.setStatus(Status.ARRIVAL);
  		if (dueOut.contains(allReservations.get(res)))
  			cleaning.setStatus(Status.DUE_OUT);
  		cleanings.add(cleaning);
  		maidIndex++;
  	}

  	cleanings = sortByMaid(cleanings);

  	return cleanings;
  }*/

  /*private List<Reservation> sortByRoom(List<Reservation> allReservations) {
  	Collections.sort(allReservations, new Comparator<Reservation>() {
  		public int compare(Reservation o1, Reservation o2) {
  			if (o1.getRoom().getId() == o2.getRoom().getId())
  				return 0;
  			return o1.getRoom().getId() < o2.getRoom().getId() ? -1 : 1;
  		}
  	});
  	return allReservations;
  }*/

  /*private List<Reservation> filterByDirty(List<Reservation> allReservations) {
  	List<Reservation> reservations = new ArrayList<>();
  	for (int i = 0; i < allReservations.size(); i++) {
  		if (allReservations.get(i).getRoom().getClean().equals(false))
  			reservations.add(allReservations.get(i));
  	}
  	return reservations;
  }*/

  /*private List<CleaningDto> sortByMaid(List<CleaningDto> cleanings) {
  	Collections.sort(cleanings, new Comparator<CleaningDto>() {
  		public int compare(CleaningDto o1, CleaningDto o2) {
  			if (o1.getMaid().getId() == o2.getMaid().getId())
  				return 0;
  			return o1.getMaid().getId() < o2.getMaid().getId() ? -1 : 1;
  		}
  	});
  	return cleanings;
  }*/

  public void newCleaning(List<Cleaning> cleaning) {
    for (int i = 0; i < cleaning.size(); i++) {
      cleaning.get(i).setDate(LocalDate.now());
      repository.save(cleaning.get(i));
    }
  }

  public boolean checkIfGenerated() {
    List<Cleaning> allCleanings = repository.findAll();
    LocalDate today = LocalDate.now();
    for (int i = 0; i < allCleanings.size(); i++) {
      if (allCleanings.get(i).getDate().equals(today)) return true;
    }
    return false;
  }

  public List<CleaningDto> getToday() {
    List<Cleaning> allCleanings = repository.findAll();
    LocalDate date = LocalDate.now();

    List<Reservation> arrival =
        reservationService.reservationsByStatusAndDate(Status.ARRIVAL, date);
    List<Reservation> dueOut = reservationService.reservationsByStatusAndDate(Status.DUE_OUT, date);
    List<Reservation> departured =
        reservationService.reservationsByStatusAndDate(Status.DEPARTURED, date);
    List<Reservation> stayover =
        reservationService.reservationsByStatusAndDate(Status.STAYOVER, date);
    List<Reservation> vacant = reservationService.reservationsByStatusAndDate(Status.VACANT, date);

    List<CleaningDto> cleanings = new ArrayList<>();

    for (int i = 0; i < stayover.size(); i++) {
      CleaningDto res =
          new CleaningDto(null, stayover.get(i), date, false, false, false, true, false, null);
      System.out.println("Stayover: " + stayover.get(i).getRoom().getId());
      cleanings.add(res);
    }
    for (int i = 0; i < departured.size(); i++) {
      CleaningDto res =
          new CleaningDto(null, departured.get(i), date, false, false, true, false, false, null);
      System.out.println("departured: " + departured.get(i).getRoom().getId());
      cleanings.add(res);
    }
    for (int i = 0; i < dueOut.size(); i++) {
      CleaningDto res =
          new CleaningDto(null, dueOut.get(i), date, false, true, false, false, false, null);
      System.out.println("dueOut: " + dueOut.get(i).getRoom().getId());
      cleanings.add(res);
    }
    for (int i = 0; i < vacant.size(); i++) {
      CleaningDto res =
          new CleaningDto(null, vacant.get(i), date, false, false, false, false, true, null);
      System.out.println("vacant: " + vacant.get(i).getRoom().getId());
      cleanings.add(res);
    }
    for (int i = 0; i < arrival.size(); i++) {
      boolean check = true;
      for (int j = 0; j < cleanings.size(); j++) {
        if (arrival.get(i).getRoom().getId()
            == cleanings.get(j).getReservation().getRoom().getId()) {
          cleanings.get(j).setArrival(true);
          check = false;
        }
      }
      if (check) {
        CleaningDto res =
            new CleaningDto(null, arrival.get(i), date, true, false, false, false, false, null);
        cleanings.add(res);
      }
    }

    for (int i = 0; i < cleanings.size(); i++) {
      for (int j = 0; j < allCleanings.size(); j++) {
        if (cleanings.get(i).getReservation().getRoom().getId()
                == allCleanings.get(j).getRoom().getId()
            && allCleanings.get(j).getDate().equals(date)) {
          cleanings.get(i).setMaid(allCleanings.get(j).getMaid());
          cleanings.get(i).setMaidNote(allCleanings.get(j).getNote());
        }
      }
    }

    cleanings = sortByRoom(cleanings);
    return cleanings;
  }

  public List<CleaningDto> getByDate(LocalDate date) {
    List<Cleaning> allCleanings = repository.findAll();
    List<Reservation> reservations = reservationService.getByDate(date);

    List<CleaningDto> cleanings = new ArrayList<>();

    for (int i = 0; i < reservations.size(); i++) {
      CleaningDto res =
          new CleaningDto(null, reservations.get(i), date, false, false, false, false, false, null);
      cleanings.add(res);
    }

    for (int i = 0; i < cleanings.size(); i++) {
      for (int j = 0; j < allCleanings.size(); j++) {
        if (cleanings.get(i).getReservation().getRoom().getId()
                == allCleanings.get(j).getRoom().getId()
            && allCleanings.get(j).getDate().equals(date)) {
          cleanings.get(i).setMaid(allCleanings.get(j).getMaid());
          cleanings.get(i).setMaidNote(allCleanings.get(j).getNote());
        }
      }
    }

    cleanings = sortByRoom(cleanings);
    return cleanings;
  }

  private List<CleaningDto> sortByRoom(List<CleaningDto> cleanings) {
    Collections.sort(
        cleanings,
        new Comparator<CleaningDto>() {
          public int compare(CleaningDto o1, CleaningDto o2) {
            if (o1.getReservation().getRoom().getId() == o2.getReservation().getRoom().getId())
              return 0;
            return o1.getReservation().getRoom().getId() < o2.getReservation().getRoom().getId()
                ? -1
                : 1;
          }
        });
    return cleanings;
  }

  public ResponseEntity<Cleaning> update(Cleaning cleaning) {
    List<Cleaning> allCleanings = repository.findAll();
    for (int j = 0; j < allCleanings.size(); j++) {
      if (cleaning.getRoom().getId() == allCleanings.get(j).getRoom().getId()
          && allCleanings.get(j).getDate().equals(LocalDate.now())) {
        allCleanings.get(j).setNote(cleaning.getNote());
        repository.save(allCleanings.get(j));
        return new ResponseEntity<Cleaning>(cleaning, HttpStatus.OK);
      }
    }
    return new ResponseEntity<Cleaning>(cleaning, HttpStatus.BAD_REQUEST);
  }

  public List<Cleaning> findByDate(LocalDate date) {
    return repository.findByDate(date);
  }
}
