package com.ivana.hotel.service;

import com.ivana.hotel.model.Employee;
import com.ivana.hotel.repository.EmployeeRepository;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class EmployeeService {

  @Autowired private EmployeeRepository repository;
  @Autowired PasswordEncoder encoder;

  public List<Employee> getMaids() {
    List<Employee> allEmployees = repository.findAll();
    List<Employee> maids = new ArrayList<>();
    for (int i = 0; i < allEmployees.size(); i++) {
      if (allEmployees.get(i).getPosition().equals(Employee.Position.MAID))
        maids.add(allEmployees.get(i));
    }
    return maids;
  }

  public List<Employee> findAll() {
    return repository.findAll();
  }

  public void deleteById(int id) {
    repository.deleteById(id);
  }

  public List<Employee> filter(String active, String position) {
    List<Employee> allEmployees = repository.findAll();
    List<Employee> returnEmployees = new ArrayList<>();

    if (allEmployees.isEmpty()) return returnEmployees;

    for (int i = 0; i < allEmployees.size(); i++) {
      if (filter(allEmployees.get(i), active, position)) returnEmployees.add(allEmployees.get(i));
    }
    return returnEmployees;
  }

  private boolean filter(Employee employee, String active, String position) {
    boolean check = true;

    if (active.equals("true") && !employee.getActive().equals(true)) check = false;
    if (active.equals("false") && !employee.getActive().equals(false)) check = false;
    if (position.equals("HOUSEKEEPING")
        && !employee.getPosition().equals(Employee.Position.HOUSEKEEPING)) check = false;
    if (position.equals("ADMINISTRATION")
        && !employee.getPosition().equals(Employee.Position.ADMINISTRATION)) check = false;
    if (position.equals("MAID") && !employee.getPosition().equals(Employee.Position.MAID))
      check = false;
    if (position.equals("RECEPTION") && !employee.getPosition().equals(Employee.Position.RECEPTION))
      check = false;

    return check;
  }

  public Employee update(Employee employee) {
    return repository.save(employee);
  }

  public ResponseEntity<Employee> register(String username, String password) {
    Employee employee = repository.findByUsername(username);
    if (employee != null) {
      if (employee.getPassword() == null) {
        employee.setPassword(encoder.encode(password));
        repository.save(employee);
        return new ResponseEntity<Employee>(HttpStatus.OK);
      } else {
        return new ResponseEntity<Employee>(HttpStatus.UNAUTHORIZED);
      }
    } else {
      return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
    }
  }
}
