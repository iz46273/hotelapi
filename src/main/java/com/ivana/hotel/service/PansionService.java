package com.ivana.hotel.service;

import com.ivana.hotel.model.Pansion;
import com.ivana.hotel.repository.PansionRepository;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class PansionService {

  @Autowired private PansionRepository repository;

  public List<Pansion> findAll() {
    return repository.findAll();
  }

  public void save(Pansion pansion) {
    repository.save(pansion);
  }
}
