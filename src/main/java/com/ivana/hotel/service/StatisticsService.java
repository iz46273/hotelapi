package com.ivana.hotel.service;

import com.ivana.hotel.dto.AdminStatistics;
import com.ivana.hotel.dto.HousekeepingStatistics;
import com.ivana.hotel.dto.ReceptionStatistics;
import com.ivana.hotel.model.Cleaning;
import com.ivana.hotel.model.Employee;
import com.ivana.hotel.model.Reservation;
import com.ivana.hotel.model.Room;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class StatisticsService {

  @Autowired private RoomService roomService;
  @Autowired private ReservationService reservationService;
  @Autowired private EmployeeService employeeService;
  @Autowired private CleaningService cleaningService;

  public AdminStatistics getAdmin() {
    AdminStatistics adminStatistics = new AdminStatistics();
    LocalDate today = LocalDate.now();

    adminStatistics.setCurrentOccupancy(getCurrentOccupancy());
    adminStatistics.setMonthlyOccupancy(getMonthlyOccupancy(today));
    adminStatistics.setMonthlyOccupancyDifference(getMonthlyOccupancyDifference());
    adminStatistics.setMonthlyEarnings(getEarningsInPeriod(today, 30));
    adminStatistics.setMonthlyEarningsDifference(getEarningsInPeriodDifference(30));
    adminStatistics.setYearlyEarnings(getEarningsInPeriod(today, 365));
    adminStatistics.setYearlyEarningsDifference(getEarningsInPeriodDifference(365));
    adminStatistics.setAverageNightsPerStay(getAverageNightsPerStay());

    return adminStatistics;
  }

  public HousekeepingStatistics getHousekeeping() {
    HousekeepingStatistics stats = new HousekeepingStatistics();

    stats.setAllMaids(employeeService.getMaids().size());
    stats.setMaidsWorkingToday(getMaidsWorkingToday());
    stats.setAllRoomsToClean(getAllRoomsToClean());
    stats.setCleanedRooms(getCleanedRooms());

    int maidsWorkingToday = getMaidsWorkingToday();
    double averageRoomsPerMaid = 0;
    if (maidsWorkingToday > 0)
      averageRoomsPerMaid = (double) getAllRoomsToClean() / (double) maidsWorkingToday;
    stats.setAverageRoomsPerMaid(averageRoomsPerMaid);

    stats.setArrivalPercentage(getArrivalPercentage());
    stats.setCheckedOutPercentage(getCheckedOutPercentage());

    return stats;
  }

  public ReceptionStatistics getReception() {
    ReceptionStatistics stats = new ReceptionStatistics();

    stats.setArrivalPercentage(getArrivalPercentage());
    stats.setCheckedOutPercentage(getCheckedOutPercentage());
    stats.setAvailableRooms(getAvailableRooms());
    stats.setOccupied(getCurrentOccupancy());
    stats.setToArrive(getToArrive());
    stats.setDueOut(getDueOut());
    stats.setGuestsInHotel(getGuestsInHotel());

    int cleanedPercentage = 100;
    if (getAllRoomsToClean() > 0)
      cleanedPercentage =
          (int) Math.round((double) getCleanedRooms() / (double) getAllRoomsToClean());
    stats.setCleanedPercentage(cleanedPercentage);

    return stats;
  }

  private double getCurrentOccupancy() {
    List<Room> allRooms = roomService.findAll();
    List<Room> occupied =
        roomService.getOccupiedRooms(LocalDate.now(), LocalDate.now().plusDays(1));

    double result = (double) occupied.size() / (double) allRooms.size() * 100;
    return Math.round(result * 100.0) / 100.0;
  }

  private double getMonthlyOccupancy(LocalDate from) {
    List<Room> allRooms = roomService.findAll();
    LocalDate to = from.plusDays(1);

    double sum = 0.0;

    for (int i = 0; i < 30; i++) {
      List<Room> occupied = roomService.getOccupiedRooms(from, to);
      double number = (double) occupied.size() / (double) allRooms.size() * 100;
      sum += number;
      from = from.minusDays(1);
      to = to.minusDays(1);
    }

    double result = sum / 30;
    return Math.round(result * 100.0) / 100.0;
  }

  private double getMonthlyOccupancyDifference() {
    double currentMonth = getMonthlyOccupancy(LocalDate.now());
    double previousMonth = getMonthlyOccupancy(LocalDate.now().minusDays(30));

    return calculateDifference(previousMonth, currentMonth);
  }

  private double getEarningsInPeriod(LocalDate from, int numberOfDays) {
    List<Reservation> monthlyResrvations =
        reservationService.getReservationsInPeriod(from, numberOfDays);
    double earnings = 0.0;

    for (int i = 0; i < monthlyResrvations.size(); i++) {
      earnings += monthlyResrvations.get(i).getTotalPrice();
    }

    return Math.round(earnings * 100.0) / 100.0;
  }

  private double getEarningsInPeriodDifference(int numberOfDays) {
    double current = getEarningsInPeriod(LocalDate.now(), numberOfDays);
    double previous = getEarningsInPeriod(LocalDate.now().minusDays(numberOfDays), numberOfDays);

    return calculateDifference(previous, current);
  }

  private double getAverageNightsPerStay() {
    List<Reservation> monthlyResrvations =
        reservationService.getReservationsInPeriod(LocalDate.now(), 30);
    int sum = 0;

    for (int i = 0; i < monthlyResrvations.size(); i++) {
      sum += monthlyResrvations.get(i).getNumberOfNights();
    }

    double result = (double) sum / (double) monthlyResrvations.size();
    return Math.round(result * 100.0) / 100.0;
  }

  private double calculateDifference(double firstValue, double secondValue) {
    double diff;
    double percentage;

    if (firstValue > 0 && secondValue > 0 && secondValue > firstValue) {
      diff = secondValue - firstValue;
      percentage = diff / firstValue * 100;

    } else if (firstValue > 0 && secondValue > 0 && secondValue < firstValue) {
      diff = secondValue - firstValue;
      percentage = diff / firstValue * 100;

    } else if (firstValue > 0 && secondValue == 0) {
      diff = secondValue - firstValue;
      percentage = diff / firstValue * 100;

    } else if (firstValue == 0 && secondValue > 0) {
      diff = secondValue - firstValue;
      percentage = diff / secondValue * 100;

    } else if (firstValue == 0 && secondValue == 0) {
      diff = 0;
      percentage = 0 * 100;
    } else percentage = 0;

    return Math.round(percentage * 100.0) / 100.0;
  }

  private int getMaidsWorkingToday() {
    List<Cleaning> allCleanings = cleaningService.findByDate(LocalDate.now());
    List<Employee> maids = new ArrayList<>();

    for (int i = 0; i < allCleanings.size(); i++) {
      Employee maid = allCleanings.get(i).getMaid();
      boolean check = true;

      for (int j = 0; j < maids.size(); j++) {
        if (maids.get(j).equals(maid)) check = false;
      }

      if (check) maids.add(maid);
    }
    return maids.size();
  }

  private int getAllRoomsToClean() {
    List<Cleaning> allCleanings = cleaningService.findByDate(LocalDate.now());
    List<Room> rooms = new ArrayList<>();

    for (int i = 0; i < allCleanings.size(); i++) {
      rooms.add(allCleanings.get(i).getRoom());
    }
    return rooms.size();
  }

  private int getCleanedRooms() {
    List<Cleaning> allCleanings = cleaningService.findByDate(LocalDate.now());
    List<Room> rooms = new ArrayList<>();

    for (int i = 0; i < allCleanings.size(); i++) {
      if (allCleanings.get(i).getRoom().getClean().equals(true))
        rooms.add(allCleanings.get(i).getRoom());
    }
    return rooms.size();
  }

  private int getArrivalPercentage() {
    List<Reservation> allReservations = reservationService.findAll();

    int toArrive = 0;
    int arrived = 0;
    int result = 100;

    for (int i = 0; i < allReservations.size(); i++) {
      if (allReservations.get(i).getCheckInDate().equals(LocalDate.now())
          && !allReservations.get(i).getStatus().equals(Reservation.Status.CANCELED)) {
        toArrive++;
        if (allReservations.get(i).getStatus() == Reservation.Status.CHECKEDIN) arrived++;
      }
    }

    if (toArrive > 0) result = (int) Math.round((double) arrived / (double) toArrive * 100);
    return result;
  }

  private int getCheckedOutPercentage() {
    List<Reservation> allReservations = reservationService.findAll();

    int dueOut = 0;
    int departured = 0;
    int result = 100;

    for (int i = 0; i < allReservations.size(); i++) {
      if (allReservations.get(i).getCheckOutDate().equals(LocalDate.now())
          && !allReservations.get(i).getStatus().equals(Reservation.Status.CANCELED)) {
        dueOut++;
        if (allReservations.get(i).getStatus() == Reservation.Status.CHECKEDOUT) departured++;
        System.out.println("dueOut" + dueOut);
        System.out.println("departured" + departured);
      }
    }

    if (dueOut > 0) result = (int) Math.round((double) departured / (double) dueOut * 100);
    System.out.println("result" + result);
    return result;
  }

  private int getAvailableRooms() {
    List<Room> allRooms = roomService.findAll();
    List<Room> occupiedRooms =
        roomService.getOccupiedRooms(LocalDate.now(), LocalDate.now().plusDays(1));

    return allRooms.size() - occupiedRooms.size();
  }

  private int getToArrive() {
    List<Reservation> allReservations = reservationService.findAll();

    int toArrive = 0;

    for (int i = 0; i < allReservations.size(); i++) {
      if (allReservations.get(i).getCheckInDate().equals(LocalDate.now())
          && allReservations.get(i).getStatus().equals(Reservation.Status.BOOKED)) {
        toArrive++;
      }
    }
    return toArrive;
  }

  private int getDueOut() {
    List<Reservation> allReservations = reservationService.findAll();

    int dueOut = 0;

    for (int i = 0; i < allReservations.size(); i++) {
      if (allReservations.get(i).getCheckOutDate().equals(LocalDate.now())
          && allReservations.get(i).getStatus().equals(Reservation.Status.CHECKEDIN)) {
        dueOut++;
      }
    }
    return dueOut;
  }

  private int getGuestsInHotel() {
    List<Reservation> allReservations = reservationService.findAll();

    int guests = 0;

    for (int i = 0; i < allReservations.size(); i++) {
      if (allReservations.get(i).getStatus().equals(Reservation.Status.CHECKEDIN)) {
        int sum =
            allReservations.get(i).getAdults()
                + allReservations.get(i).getChildren()
                + allReservations.get(i).getBabies();
        guests += sum;
      }
    }
    return guests;
  }
}
